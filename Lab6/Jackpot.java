import java.util.Scanner;
public class Jackpot{
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		boolean anOtherGame = true;
		int numOfGameWon = 0;
		
		while(anOtherGame == true){
		
			System.out.println("Welcome to the game!");
		
			Board board = new Board();
		
			boolean gameOver = false;
		
			int numOfTilesClosed = 0;
		
			while(gameOver == false){
			
				System.out.println(board);
			
				if(board.playATurn()){
				
					gameOver=true;
				
				}else{
				
					numOfTilesClosed = numOfTilesClosed +1;
				
				}
			}
			
			if(numOfTilesClosed >= 7){
				
				System.out.println("You won the Jackpot!");
				System.out.println("You closed a total of "+numOfTilesClosed);
				numOfGameWon = numOfGameWon +1;
				
			}else{
				
				System.out.println("You lost!");
				System.out.println("You closed a total of "+numOfTilesClosed);
				
			}
			
			System.out.println("Do you want to play an other game? Yes or No.");
			String answerOtherGame = sc.next();
			if (answerOtherGame.equals("Yes")){
				
				anOtherGame=true;
			
			}else{
				
				System.out.println("You in total won "+numOfGameWon+"! !Congratulations!");
				System.out.println("Have a Nice Day!");
				anOtherGame=false;
			
			}
		}
		
	}
}