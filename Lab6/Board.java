public class Board{
	
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	
	//Constructor
	public Board(){
		
		this.die1 = new Die();
		this.die2 = new Die();
		this.closedTiles = new boolean[12];
		
	}
	
	public String toString(){
		
		String tiles = "";
		
		for(int i = 0; i<this.closedTiles.length; i++){
			
			if(this.closedTiles[i] == false){
				
			tiles += i+1+" ";
			
			}else{
			
				tiles += "X ";
			
			}
		}
		
		return "Board representation: "+ tiles;
	}
	
	//PlayATurn method
	public boolean playATurn(){
		
		this.die1.roll();
		this.die2.roll();

		System.out.println(die1);
		System.out.println(die2);
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		if(this.closedTiles[sumOfDice-1] == false){
			
			this.closedTiles[sumOfDice-1] = true;
			System.out.println("Closing tile: "+ sumOfDice);
			return false;
			
		}else if(this.closedTiles[die1.getFaceValue()] == false){
			
			this.closedTiles[die1.getFaceValue()] = true;
			System.out.println("Closing tile: "+ die1);
			return false;
			
		}else if(this.closedTiles[die2.getFaceValue()] == false){
			
			this.closedTiles[die2.getFaceValue()] = true;
			System.out.println("Closing tile: "+ die2);
			return false;
			
		}else{
			
		System.out.println("All the tiles for these values are already shut");
		return true;
		}
	}
}