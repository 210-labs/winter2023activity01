import java.util.Random;
public class Die{
	
	private int faceValue;
	private Random r;
	
	//Constructor
	public Die(){
		
		this.faceValue = 1;
		this.r = new Random();
		
	}
	
	//Get Methods
	public int getFaceValue(){
		
		return this.faceValue;
		
	}
	
	public Random getRandom(){
		
		return this.r;
		
	}
	
	public void roll(){
		
		this.faceValue = r.nextInt(6)+1;
		
	}
	
	public String toString(){
		
		return "You just rolled a " +this.faceValue;
		
	}
	
}