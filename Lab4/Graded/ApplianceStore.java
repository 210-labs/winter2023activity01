import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
		Scanner scInt = new Scanner(System.in);
		
		Toaster[] toaster = new Toaster[4];
		
			for(int i = 0;i<toaster.length;i++){
				
					System.out.println("What brand is the toaster?");
					String brand = sc.nextLine();
					
					System.out.println("What color is the Toaster?");
					String color = sc.nextLine();
					
					System.out.println("How many slots does the Toaster have?");
					int numberOfSlots = scInt.nextInt();
					
					System.out.println("What is the heat level?");
					int heatLevel = scInt.nextInt();
	
					toaster[i] = new Toaster(brand,color,numberOfSlots,heatLevel);
	
			}
			
		toaster[3].heatLevel(toaster[3].getIntHeatLevel());
		
		System.out.println();	
		System.out.println("The latest toaster is ");
		System.out.println(toaster[3].getBrand());
		System.out.println(toaster[3].getColor());
		System.out.println(toaster[3].getNumberOfSlots());
		System.out.println(toaster[3].getHeatLevel());
		System.out.println();
			
			System.out.println("Change latest toaster information: ");
			toaster[3].setBrand(sc.nextLine()); 
			toaster[3].setColor(sc.nextLine());
			toaster[3].setNumberOfSlots(sc.nextInt());
			toaster[3].setIntHeatLevel(sc.nextInt());
			toaster[3].heatLevel(toaster[3].getIntHeatLevel());
			
		
		System.out.println();	
		System.out.println("The latest toaster is ");
		System.out.println(toaster[3].getBrand());
		System.out.println(toaster[3].getColor());
		System.out.println(toaster[3].getNumberOfSlots());
		System.out.println(toaster[3].getHeatLevel());
		System.out.println();
		
		toaster[0].printBrand();
		toaster[0].speedOfToaster();
		
		
		toaster[2].heatLevel(toaster[2].getIntHeatLevel());
		System.out.println("What is the heat level of the 3rd toaster? " +toaster[2].getHeatLevel());
	
	
	
	}
}