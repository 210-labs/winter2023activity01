public class Toaster{
	
	private String brand;
	private String color;
	private int numberOfSlots;
	private int intHeatLevel;
	private String heatLevel;
	
	//constructor
	
	public Toaster(String brand, String color, int numberOfSlots, int intHeatLevel){
		
		this.brand = brand;
		this.color = color;
		this.numberOfSlots = numberOfSlots;
		this.intHeatLevel = intHeatLevel;
		
	}
	
	public void printBrand(){
		
		System.out.println("The brand of this toaster is "+brand);
		
	}
	
	public void speedOfToaster(){
		
		System.out.println("The toaster will toast "+numberOfSlots+"X faster.");
		
	}
	
	public void heatLevel(int heatLevel){
		
		if(heatLevel<=0||heatLevel>10){
			
			this.heatLevel = "ERROR, Value over 10 or under 1.";
		
		}else if(heatLevel<4){
			
			this.heatLevel = "Low Heat Level.";
			
		}else if(heatLevel<7){
			
			this.heatLevel = "Medium Heat Level.";
			
		}else if(heatLevel<11){
			
			this.heatLevel = "High Heat Level.";
			
		}
		
	}
	

	//Setter Methods
	
	public void setBrand(String newBrand){
		
		this.brand = newBrand;
		
	}
	
	public void setColor(String newColor){
		
		this.color = newColor;
		
	}
	
	public void setNumberOfSlots(int newNumberOfSlots){
		
		this.numberOfSlots = newNumberOfSlots;
		
	}
	
	public void setIntHeatLevel(int newIntHeatLevel){
		
		this.intHeatLevel = newIntHeatLevel;
		
	}
	
	
	//Getter Methods
	
	public String getBrand(){
		
		return this.brand;
		
	}
	
	public String getColor(){
		
		return this.color;
		
	}
	
	public int getNumberOfSlots(){
		
		return this.numberOfSlots;
		
	}
	
	public int getIntHeatLevel(){
		
		return this.intHeatLevel;
		
	}
	
	public String getHeatLevel(){
		
		return this.heatLevel;
		
	}
	
	
}