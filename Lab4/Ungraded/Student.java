public class Student{
	
	private String studentId;
	
	private String name;
	
	private String program;
	
	public int amountLearnt;
	
	//Constructor
	
	public Student(String studentId, String name, String program){
		
		this.studentId = studentId;
		this.name = name;
		this.program = program;
		
	}
	
	public void learn(int x){
		
		if(x > 0){
			this.amountLearnt = x;
		}
	}
	
	public void sayHi(){
		
		System.out.println("Hi "+this.name);
		
	}
	
	public void info() {
		
		System.out.println("Name: "+this.name+"StudentID: "+this.studentId+"Program: "+this.program);
		
	}
	
	//setMethods
	
	public void setName(String newName){
		
		this.name = newName;
		
	}
	
	public void setStudentId(String newId){
		
		this.studentId = newId;
		
	}
	
	public void setProgram(String newProgram){
		
		this.program = newProgram;
		
	}
	
	//getMethods
	
	public String getStudentId(){
		
		return this.studentId;
		
	}
	
	public String getName(){
		
		return this.name;
		
	}
	
	public String getProgram(){
		
		return this.program;
		
	}
	
}