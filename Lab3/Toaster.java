public class Toaster{
	
	public String brand;
	public String color;
	public int numberOfSlots;
	
	public void printBrand(){
		
		System.out.println("The brand of this toaster is "+brand);
		
	}
	
	public void speedOfToaster(){
		
		System.out.println("The toaster will toast "+numberOfSlots+"X faster.");
		
	}
}