import java.nio.file.*;
import java.util.*;

public class ShapeReader {
    public static void main(String[] args) throws Exception {
		
		//Test Cone I created
		/*Cone cone1 = new Cone(3,4);
		//System.out.println(cone1.getVolume());
		System.out.println(cone1+"coneTest"); */
		
		Cone[] cones = loadTriangles("cones.csv");
        printCones(cones);
    }

    public static Cone[] loadTriangles(String path) throws Exception {
		List<String> linesAsList = Files.readAllLines(Paths.get(path));
        String[] lines = linesAsList.toArray(new String[0]);

        Cone[] cones = new Cone[lines.length];
        for (int i = 0; i < lines.length; i++) {
            String[] pieces = lines[i].split(",");
			double a=Double.parseDouble(pieces[0]);
			double b=Double.parseDouble(pieces[1]);
            cones[i] = new Cone(a,b);
        }

        return cones;
    }

    public static void printCones(Cone[] cones) {
        for (Cone cone : cones) {
            System.out.println(cone);
        }
    }
}
