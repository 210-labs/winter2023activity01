public class Cone {
    private double radius;
	private double height;

    public Cone(double r, double h) {
        this.radius = r;
		this.height = h;
    }

    public double getVolume() {
        return ((1.0 / 3.0) * Math.PI * (this.radius * this.radius) * this.height);
    }

    public double getSlantHeight() {
        return Math.sqrt(this.radius * this.radius + this.height * this.height);
    }


    public String toString() {
        return "Height: " + this.height + ", Base radius: " + this.radius + " , Slant height: " + this.getSlantHeight() + " Volume: "+this.getVolume();
    }
}
